//import { Component } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { RegisteredUser } from './shared/registeredUser';
import { UserRegistrationService } from './services/userRegn.service';


/*
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FormBuilder, FormGroup } from '@angular/forms';
import { Directive, HostListener, Input, ElementRef, OnInit } from '@angular/core';
import {NgControl} from '@angular/forms';
*/
import { FormsModule, FormControl, FormGroup, FormGroupDirective, FormBuilder, NgForm, Validators } from '@angular/forms';
//import { AuthService } from '@angular/material';

import { AfterViewInit } from '@angular/core';
import { DateAdapter, NativeDateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatDatepickerModule } from "@angular/material";
import 'rxjs/add/operator/map';
import { Moment } from 'moment';
import * as moment from 'moment';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const USER_REGEX = /^[a-z\d](?:[a-z\d]|-(?=[a-z\d])){0,38}$/i;
const PASSWORD_REGEX = /^(?=.*\d)(?=.*[a-zA-Z]).{6,20}$/;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit {
  title = 'n8blerApp by AJ';

  hide = true;
  gender: string;
  sexes = [
    { value: 'male', viewValue: 'Male' },
    { value: 'female', viewValue: 'Female' }
  ];

  countries = [
    { value: 'Singaporean', viewValue: 'SG - Singapore' },
    { value: 'Malaysian', viewValue: 'MY - Malaysia' },
    { value: 'Thai', viewValue: 'TH - Thailand' },
    { value: 'Vietnamese', viewValue: 'VN - Thailand' }
  ];

  
  isSubmitted: boolean = false;
  users: any;
  model: any;

  regForm: NgForm;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  passwordFormControl = new FormControl('', [
    Validators.required,
  ]);
  confirmPasswordFormControl = new FormControl('', [
    Validators.required,
  ]);
  nameFormControl = new FormControl('', [
    Validators.required,
  ]);
  mobileFormControl = new FormControl('', [
    Validators.required,
  ]);
  DOBFormControl = new FormControl('', [
    Validators.required,
  ]);
  genderFormControl = new FormControl('', [
    Validators.required,
  ]);
  postalCodeFormControl = new FormControl('', [
    Validators.required,
  ]);
  addressFormControl = new FormControl('', [
    Validators.required,
  ]);
  countryFormControl = new FormControl('', [
    Validators.required,
  ]);

  constructor(private userService: UserRegistrationService){
  }

  // regnForm: FormGroup;


  // when page loads
  ngOnInit() {
    this.model = new RegisteredUser('', '', '', '', '', null, '', '', '', 23);
  }


onSubmitForm()
{


  this.model.email = this.emailFormControl;
  this.model.password = this.passwordFormControl;
  this.model.confirmPassword = this.confirmPasswordFormControl;
  this.model.name = this.nameFormControl;
  this.model.mobile = this.mobileFormControl;
  this.model.DOB = this.DOBFormControl;
  this.model.gender = this.genderFormControl;
  this.model.address = this.addressFormControl;
  this.model.country = this.countryFormControl;
  this.model.gender = this.genderFormControl;

  console.log(this.model.email);
  console.log(this.model.password);
  console.log(this.model.confirmPassword);
  console.log(this.model.name);
  console.log(this.model.mobile);
  console.log(this.model.DOB);
  console.log(this.model.gender);
  console.log(this.model.address);
  console.log(this.model.country);
  
  this.userService.saveUserRegistration(this.model)
    .subscribe(users => {
      console.log('send to backend !');
      console.log(users);
      this.users = users;
    })
  this.isSubmitted = true;

}

}


